//Khai báo thư viện express:
const express = require('express');
const {Company} = require("../middleware/middlewareCompany")
//khoi tao router
const companyRouter = express.Router();


companiesArr = [
    new Company(1, "Alfreds Futterkiste", "Maria Anders", "Germany"),
    new Company(2, "Centro comercial Moctezuma", "Francisco Chang", "Mexico"),
    new Company(3, "Ernst Handel", "Roland Mendel", "Austria"),
    new Company(4, "Island Trading", "Helen Bennett", "UK"),
    new Company(5, "Laughing Bacchus Winecellars", "Yoshi Tannamuri", "Canada"),
    new Company(6, "Magazzini Alimentari Riuniti", "Giovanni Rovelli", "Italy")
];

// companyRouter.get('/companies', (req, res) => {
//     console.log(`Get all companies`);
//     res.json({
//         companiesArr
//     })
// })
companyRouter.get('/companies', (req, res) => {
    const companyId = req.query.code;
    console.log(`Get company by id: ${companyId}`);

    if (companyId) {
        let getCompanyById = companiesArr.filter(item => item.id == companyId);
        res.json(
            getCompanyById
        )
        
    } else {
        res.json(
            companiesArr
        )
    }

})

module.exports = {companyRouter}

